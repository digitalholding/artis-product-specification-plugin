<?php

namespace Tests\DH\ArtisProductSpecificationPlugin\Application\src\Entity;

use DH\Artis\Common\Entity\Product\ProductInterface as BaseProductInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductSpecificationAwareInterface;

interface ProductInterface extends BaseProductInterface,
    ProductSpecificationAwareInterface
{
}
