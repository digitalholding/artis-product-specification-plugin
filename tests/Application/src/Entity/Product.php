<?php

declare(strict_types=1);

namespace Tests\DH\ArtisProductSpecificationPlugin\Application\src\Entity;

use DH\Artis\Common\Entity\Product\Product as BaseProduct;
use DH\ArtisProductSpecificationPlugin\Entity\ProductSpecificationAwareTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product")
 */
class Product extends BaseProduct implements ProductInterface
{
    use ProductSpecificationAwareTrait;
}
