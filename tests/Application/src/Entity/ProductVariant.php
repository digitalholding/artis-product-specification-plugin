<?php

namespace Tests\DH\ArtisProductSpecificationPlugin\Application\src\Entity;

use DH\Artis\Common\Entity\Product\ProductVariant as BaseProductVariant;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationAwareTrait;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationImagesAwareTrait;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValuesAwareTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product_variant")
 */
class ProductVariant extends BaseProductVariant implements ProductVariantInterface
{
    use ProductVariantSpecificationAwareTrait;
    use ProductVariantSpecificationImagesAwareTrait;
    use ProductVariantSpecificationItemValuesAwareTrait;

    public function __construct()
    {
        parent::__construct();

        $this->initializeVariantSpecificationImageCollection();
        $this->initializeVariantSpecificationItemValuesCollection();
    }
}
