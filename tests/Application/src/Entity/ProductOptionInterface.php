<?php

namespace Tests\DH\ArtisProductSpecificationPlugin\Application\src\Entity;

use DH\Artis\Common\Entity\Product\ProductOptionInterface as BaseProductOptionInterface;

interface ProductOptionInterface extends BaseProductOptionInterface
{
}
