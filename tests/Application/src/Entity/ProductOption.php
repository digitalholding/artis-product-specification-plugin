<?php

declare(strict_types=1);

namespace Tests\DH\ArtisProductSpecificationPlugin\Application\src\Entity;

use DH\Artis\Common\Entity\Product\ProductOption as BaseProductOption;
use DH\ArtisProductSpecificationPlugin\Entity\ProductOptionTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product_option")
 */
class ProductOption extends BaseProductOption implements ProductOptionInterface
{
    use ProductOptionTrait;

    public function __construct()
    {
        parent::__construct();
    }
}
