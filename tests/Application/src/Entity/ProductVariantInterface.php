<?php

declare(strict_types=1);

namespace Tests\DH\ArtisProductSpecificationPlugin\Application\src\Entity;

use DH\Artis\Common\Entity\Product\ProductVariantInterface as BaseProductVariantInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationImagesAwareInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValuesAwareInterface;

interface ProductVariantInterface extends BaseProductVariantInterface,
    ProductVariantSpecificationImagesAwareInterface,
    ProductVariantSpecificationItemValuesAwareInterface
{
}
