<?php

declare(strict_types=1);

namespace Tests\DH\ArtisProductSpecificationPlugin\Application\src\View;

use DH\Artis\Common\ShopApi\View\Product\ProductVariantView as BaseProductVariantView;
use DH\ArtisProductSpecificationPlugin\ShopApi\View\ProductVariantSpecificationViewAwareTrait;

class ProductVariantView extends BaseProductVariantView
{
    use ProductVariantSpecificationViewAwareTrait;
}
