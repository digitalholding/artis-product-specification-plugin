<?php

declare(strict_types=1);

namespace Tests\DH\ArtisProductSpecificationPlugin\Application\src\View;

use DH\Artis\Common\ShopApi\View\Taxon\TaxonView as BaseTaxonView;
use DH\ArtisProductSpecificationPlugin\ShopApi\View\TaxonSpecificationViewAwareTrait;

class TaxonView extends BaseTaxonView
{
    use TaxonSpecificationViewAwareTrait;
}
