<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Factory;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItem;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValue;
use Sylius\Component\Resource\Factory\FactoryInterface;

final class ProductVariantSpecificationItemValueFactory implements ProductVariantSpecificationItemValueFactoryInterface
{
    /** @var FactoryInterface */
    private $baseFactory;

    public function __construct(
        FactoryInterface $baseFactory
    ) {
        $this->baseFactory = $baseFactory;
    }

    public function createWithItem(ProductVariantSpecificationItem $item): object
    {
        /** @var ProductVariantSpecificationItemValue $itemValue */
        $itemValue = $this->createNew();
        $itemValue->setItem($item);

        return $itemValue;
    }

    public function createNew(): object
    {
        return $this->baseFactory->createNew();
    }
}
