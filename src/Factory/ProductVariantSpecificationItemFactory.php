<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Factory;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItem;
use Ramsey\Uuid\Uuid;
use Sylius\Component\Resource\Factory\FactoryInterface;

final class ProductVariantSpecificationItemFactory implements ProductVariantSpecificationItemFactoryInterface
{
    /** @var FactoryInterface */
    private $baseFactory;

    public function __construct(
        FactoryInterface $baseFactory
    ) {
        $this->baseFactory = $baseFactory;
    }

    /**
     * @return object
     */
    public function createNew()
    {
        return $this->baseFactory->createNew();
    }

    public function createNewWithType(string $type, ?string $code): object
    {
        /** @var ProductVariantSpecificationItem $variantSpecificationItem */
        $variantSpecificationItem = $this->createNew();
        $variantSpecificationItem->setType(strtoupper($type));
        if ($code === null) {
            $code = Uuid::uuid4()->toString();
        }
        $variantSpecificationItem->setCode($code);

        return $variantSpecificationItem;
    }
}
