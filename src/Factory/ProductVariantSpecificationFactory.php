<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Factory;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface;
use Ramsey\Uuid\Uuid;
use Sylius\Component\Resource\Factory\FactoryInterface;

final class ProductVariantSpecificationFactory implements ProductVariantSpecificationFactoryInterface
{
    /** @var FactoryInterface */
    private $baseFactory;

    public function __construct(
        FactoryInterface $baseFactory
    ) {
        $this->baseFactory = $baseFactory;
    }

    public function createWithCode(): object
    {
        /** @var ProductVariantSpecificationInterface $variantSpecification */
        $variantSpecification = $this->createNew();
        $variantSpecification->setCode(Uuid::uuid4()->toString());

        return $variantSpecification;
    }

    public function createNew(): object
    {
        return $this->baseFactory->createNew();
    }
}
