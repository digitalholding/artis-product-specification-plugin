<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Factory;

use Sylius\Component\Resource\Factory\FactoryInterface;

interface ProductVariantSpecificationFactoryInterface extends FactoryInterface
{
    public function createWithCode(): object;
}
