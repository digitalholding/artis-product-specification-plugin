<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Factory;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItem;
use DH\ArtisProductSpecificationPlugin\Entity\Translation\ProductVariantSpecificationItemTranslation;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

class ProductVariantSpecificationItemTranslationFactory implements ProductVariantSpecificationItemTranslationFactoryInterface
{
    /** @var FactoryInterface */
    private $baseFactory;

    /** @var ChannelContextInterface */
    private $locale;

    public function __construct(
        FactoryInterface $baseFactory,
        ChannelContextInterface $locale
    ) {
        $this->baseFactory = $baseFactory;
        $this->locale = $locale;
    }

    public function createNewEmpty(ProductVariantSpecificationItem $productVariantSpecificationItem): object
    {
        /** @var ProductVariantSpecificationItemTranslation $translation */
        $translation = $this->createNew();
        $translation->setName(null);
        $translation->setTranslatable($productVariantSpecificationItem);
        $translation->setLocale($this->locale->getChannel()->getDefaultLocale()->getCode());

        return $translation;
    }

    /**
     * @return object
     */
    public function createNew()
    {
        return $this->baseFactory->createNew();
    }
}
