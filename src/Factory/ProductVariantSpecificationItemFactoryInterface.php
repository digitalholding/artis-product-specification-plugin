<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Factory;

use Sylius\Component\Resource\Factory\FactoryInterface;

interface ProductVariantSpecificationItemFactoryInterface extends FactoryInterface
{
    public function createNewWithType(string $type, string $code): object;
}
