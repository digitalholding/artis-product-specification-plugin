<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Factory;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItem;
use Sylius\Component\Resource\Factory\FactoryInterface;

interface ProductVariantSpecificationItemTranslationFactoryInterface extends FactoryInterface
{
    public function createNewEmpty(ProductVariantSpecificationItem $productVariantSpecificationItem): object;
}
