<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Factory;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItem;
use Sylius\Component\Resource\Factory\FactoryInterface;

interface ProductVariantSpecificationItemValueFactoryInterface extends FactoryInterface
{
    public function createWithItem(ProductVariantSpecificationItem $item): object;
}
