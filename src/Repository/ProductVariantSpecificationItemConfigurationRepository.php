<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Repository;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemConfigurationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class ProductVariantSpecificationItemConfigurationRepository extends EntityRepository implements ProductVariantSpecificationItemConfigurationRepositoryInterface
{
    public function findBySpecificationAndSpecificationItem(
        ProductVariantSpecificationInterface $specification,
        ProductVariantSpecificationItemInterface $specificationItem
    ): ?ProductVariantSpecificationItemConfigurationInterface {
        return $this->createQueryBuilder('o')
            ->andWhere('o.specification = :specification')
            ->andWhere('o.specificationItem = :specificationItem')
            ->setParameter('specification', $specification)
            ->setParameter('specificationItem', $specificationItem)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
