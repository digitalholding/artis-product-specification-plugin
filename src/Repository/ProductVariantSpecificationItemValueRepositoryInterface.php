<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Repository;

use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface ProductVariantSpecificationItemValueRepositoryInterface extends RepositoryInterface
{
    public function getQuerySpecificationItem(int $id): QueryBuilder;
}
