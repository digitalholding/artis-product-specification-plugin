<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Repository;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValueInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValuesInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class ProductVariantSpecificationItemValuesRepository extends EntityRepository implements ProductVariantSpecificationItemValuesRepositoryInterface
{
    public function findByProductVariantAndSpecificationItemValue(
        ProductVariantInterface $productVariant,
        ProductVariantSpecificationItemValueInterface $specificationItemValue
    ): ?ProductVariantSpecificationItemValuesInterface {
        return $this->createQueryBuilder('o')
            ->andWhere('o.productVariant = :productVariant')
            ->andWhere('o.specificationItemValue = :specificationItemValue')
            ->setParameter('productVariant', $productVariant)
            ->setParameter('specificationItemValue', $specificationItemValue)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
