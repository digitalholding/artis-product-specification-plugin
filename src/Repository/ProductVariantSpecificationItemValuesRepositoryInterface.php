<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Repository;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValueInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValuesInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface ProductVariantSpecificationItemValuesRepositoryInterface extends RepositoryInterface
{
    public function findByProductVariantAndSpecificationItemValue(
        ProductVariantInterface $productVariant,
        ProductVariantSpecificationItemValueInterface $specificationItemValue
    ): ?ProductVariantSpecificationItemValuesInterface;
}
