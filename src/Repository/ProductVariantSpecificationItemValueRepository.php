<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Repository;

use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class ProductVariantSpecificationItemValueRepository extends EntityRepository implements ProductVariantSpecificationItemValueRepositoryInterface
{
    public function getQuerySpecificationItem(int $id): QueryBuilder
    {
        return $this->createQueryBuilder('s')
            ->where('s.item = :item')
            ->setParameter(':item', $id);
    }
}
