<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Repository;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemConfigurationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface ProductVariantSpecificationItemConfigurationRepositoryInterface extends RepositoryInterface
{
    public function findBySpecificationAndSpecificationItem(
        ProductVariantSpecificationInterface $specification,
        ProductVariantSpecificationItemInterface $specificationItem
    ): ?ProductVariantSpecificationItemConfigurationInterface;
}
