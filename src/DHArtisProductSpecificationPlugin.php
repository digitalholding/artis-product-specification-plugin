<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin;

use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class DHArtisProductSpecificationPlugin extends Bundle
{
    use SyliusPluginTrait;
}
