<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;

abstract class ProductSpecificationController extends ResourceController
{
}
