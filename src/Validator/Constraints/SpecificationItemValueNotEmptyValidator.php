<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Validator\Constraints;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValueInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Webmozart\Assert\Assert;

final class SpecificationItemValueNotEmptyValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        /** @var ProductVariantSpecificationItemInterface $value */
        Assert::isInstanceOf($value, ProductVariantSpecificationItemInterface::class);

        /** @var SpecificationItemValueNotEmpty $constraint */
        Assert::isInstanceOf($constraint, SpecificationItemValueNotEmpty::class);

        $fields = $constraint->fields;
        if (0 === count($fields)) {
            throw new ConstraintDefinitionException('At least one field has to be specified.');
        }

        $formData = $this->context->getRoot()->getData();

        /** @var ArrayCollection|ProductVariantSpecificationItemValueInterface[]|null */
        $specificationItemValues = $formData->getValues();

        if ((null === $specificationItemValues ||
            $specificationItemValues->isEmpty()) &&
            in_array($value->getType(), [
                ProductVariantSpecificationItemInterface::TYPE_SELECT,
                ProductVariantSpecificationItemInterface::TYPE_SELECT_WITH_IMAGE
            ])) {
            $this->context->addViolation(
                $constraint->message
            );
        }
    }
}
