<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

final class SpecificationItemValueNotEmpty extends Constraint
{
    public string $message = 'artis_product_specification_plugin.specification_item.empty_value';

    public array $fields = [];

    public function validatedBy(): string
    {
        return 'artis_product_specification_plugin_specification_item_value_not_empty';
    }

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    public function getDefaultOption(): ?string
    {
        return 'fields';
    }
}
