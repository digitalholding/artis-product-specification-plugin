<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\Factory;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use DH\ArtisProductSpecificationPlugin\ShopApi\View\VariantSpecificationItemView;

interface VariantSpecificationItemViewFactoryInterface
{
    public function create(
        ProductVariantInterface $variant,
        ProductVariantSpecificationInterface $variantSpecification,
        ProductVariantSpecificationItemInterface $variantSpecificationItem,
        string $locale
    ): VariantSpecificationItemView;
}
