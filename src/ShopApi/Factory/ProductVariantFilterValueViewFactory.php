<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\Factory;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValueInterface;
use DH\ArtisProductSpecificationPlugin\ShopApi\View\ProductVariantFilterValueView;
use Sylius\ShopApiPlugin\Factory\ImageViewFactoryInterface;

class ProductVariantFilterValueViewFactory implements ProductVariantFilterValueViewFactoryInterface
{
    /** @var ImageViewFactoryInterface */
    private $imageViewFactory;

    public function __construct(
        ImageViewFactoryInterface $imageViewFactory
    ) {
        $this->imageViewFactory = $imageViewFactory;
    }

    public function create(ProductVariantSpecificationItemValueInterface $itemValue): ProductVariantFilterValueView
    {
        /** @var ProductVariantFilterValueView $productVariantFilterValueView */
        $productVariantFilterValueView = new ProductVariantFilterValueView();

        $productVariantFilterValueView->name = $itemValue->getValue();
        $productVariantFilterValueView->code = $itemValue->getCode();
        $productVariantFilterValueView->image = $itemValue->getImage() ? $this->imageViewFactory->create($itemValue->getImage()) : null;

        return $productVariantFilterValueView;
    }
}
