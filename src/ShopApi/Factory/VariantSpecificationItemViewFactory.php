<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\Factory;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\Artis\Common\ShopApi\Factory\ImageViewFactoryInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemConfigurationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use DH\ArtisProductSpecificationPlugin\Entity\Translation\ProductVariantSpecificationItemTranslationInterface;
use DH\ArtisProductSpecificationPlugin\Repository\ProductVariantSpecificationItemConfigurationRepositoryInterface;
use DH\ArtisProductSpecificationPlugin\ShopApi\View\VariantSpecificationItemView;

class VariantSpecificationItemViewFactory implements VariantSpecificationItemViewFactoryInterface
{
    /** @var ImageViewFactoryInterface */
    private $imageViewFactory;

    /** @var ProductVariantSpecificationItemConfigurationRepositoryInterface */
    private $productVariantSpecificationItemConfigurationRepository;

    /** @var VariantSpecificationItemValueViewFactoryInterface */
    private $variantSpecificationItemValueViewFactory;

    public function __construct(
        ImageViewFactoryInterface $imageViewFactory,
        ProductVariantSpecificationItemConfigurationRepositoryInterface $productVariantSpecificationItemConfigurationRepository,
        VariantSpecificationItemValueViewFactoryInterface $variantSpecificationItemValueViewFactory
    ) {
        $this->imageViewFactory = $imageViewFactory;
        $this->productVariantSpecificationItemConfigurationRepository = $productVariantSpecificationItemConfigurationRepository;
        $this->variantSpecificationItemValueViewFactory = $variantSpecificationItemValueViewFactory;
    }

    public function create(
        ProductVariantInterface $variant,
        ProductVariantSpecificationInterface $variantSpecification,
        ProductVariantSpecificationItemInterface $variantSpecificationItem,
        string $locale
    ): VariantSpecificationItemView {
        /** @var VariantSpecificationItemView $variantSpecificationItemView */
        $variantSpecificationItemView = new VariantSpecificationItemView();

        $variantSpecificationItemView->code = $variantSpecificationItem->getCode();

        /** @var ProductVariantSpecificationItemTranslationInterface $translation */
        $translation = $variantSpecificationItem->getTranslation($locale);
        $variantSpecificationItemView->name = $translation->getName();

        /** @var ProductVariantSpecificationItemConfigurationInterface $variantSpecificationItemConfiguration */
        $variantSpecificationItemConfiguration = $this->productVariantSpecificationItemConfigurationRepository->findBySpecificationAndSpecificationItem($variantSpecification, $variantSpecificationItem);

        $variantSpecificationItemView->value = $this->variantSpecificationItemValueViewFactory->create(
            $variant,
            $variantSpecification,
            $variantSpecificationItem,
            $locale
        );
        $variantSpecificationItemView->visibleInDescription = $variantSpecificationItemConfiguration->getVisibleInDescription();
        $variantSpecificationItemView->visibleInProductList = $variantSpecificationItemConfiguration->getVisibleInProductList();
        $variantSpecificationItemView->visibleInSpecification = $variantSpecificationItemConfiguration->isVisibleInSpecification();
        $variantSpecificationItemView->usedAsFilter = $variantSpecificationItemConfiguration->isUsedAsFilter();

        if (null !== $variantSpecificationItem->getImage()) {
            $variantSpecificationItemView->image = $this->imageViewFactory->create($variantSpecificationItem->getImage());
        }

        return $variantSpecificationItemView;
    }
}
