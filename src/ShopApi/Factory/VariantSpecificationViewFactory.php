<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\Factory;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\Artis\Common\ShopApi\Factory\ImageViewFactoryInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\Translation\ProductVariantSpecificationTranslationInterface;
use DH\ArtisProductSpecificationPlugin\ShopApi\View\VariantSpecificationView;

class VariantSpecificationViewFactory implements VariantSpecificationViewFactoryInterface
{
    /** @var VariantSpecificationItemViewFactoryInterface */
    private $variantSpecificationItemViewFactory;

    /** @var ImageViewFactoryInterface */
    private $imageViewFactory;

    public function __construct(
        VariantSpecificationItemViewFactoryInterface $variantSpecificationItemViewFactory,
        ImageViewFactoryInterface $imageViewFactory
    ) {
        $this->variantSpecificationItemViewFactory = $variantSpecificationItemViewFactory;
        $this->imageViewFactory = $imageViewFactory;
    }

    public function create(
        ProductVariantSpecificationInterface $variantSpecification,
        ProductVariantInterface $variant,
        string $locale
    ): VariantSpecificationView {
        /** @var VariantSpecificationView $variantSpecificationView */
        $variantSpecificationView = new VariantSpecificationView();

        $variantSpecificationView->code = $variantSpecification->getCode();

        /** @var ProductVariantSpecificationTranslationInterface $translation */
        $translation = $variantSpecification->getTranslation($locale);

        $variantSpecificationView->name = $translation->getName();
        $variantSpecificationView->description = $translation->getDescription();

        foreach ($variant->getVariantSpecificationImages() as $variantSpecificationImage) {
            $productVariantImage = $this->imageViewFactory->createWithOriginalSize($variantSpecificationImage);

            $variantSpecificationView->images[] = $productVariantImage;
        }

        foreach ($variantSpecification->getItems() as $item) {
            $variantSpecificationView->items[] = $this->variantSpecificationItemViewFactory->create($variant, $variantSpecification, $item, $locale);
        }

        return $variantSpecificationView;
    }
}
