<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\Factory;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface;
use DH\ArtisProductSpecificationPlugin\ShopApi\View\VariantSpecificationView;

interface VariantSpecificationViewFactoryInterface
{
    public function create(
        ProductVariantSpecificationInterface $variantSpecification,
        ProductVariantInterface $variant,
        string $locale
    ): VariantSpecificationView;
}
