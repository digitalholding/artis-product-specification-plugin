<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\Factory;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValuesInterface;
use DH\ArtisProductSpecificationPlugin\ShopApi\View\ProductVariantFilterView;
use Doctrine\Common\Collections\ArrayCollection;
use Sylius\ShopApiPlugin\Factory\ImageViewFactoryInterface;

class ProductVariantFilterViewFactory implements ProductVariantFilterViewFactoryInterface
{
    /** @var ImageViewFactoryInterface */
    private $imageViewFactory;

    /** @var ProductVariantFilterValueViewFactoryInterface */
    private $productVariantFilterValueViewFactory;

    public function __construct(
        ImageViewFactoryInterface $imageViewFactory,
        ProductVariantFilterValueViewFactoryInterface $productVariantFilterValueViewFactory
    ) {
        $this->imageViewFactory = $imageViewFactory;
        $this->productVariantFilterValueViewFactory = $productVariantFilterValueViewFactory;
    }

    public function create(
        ProductVariantSpecificationItemValuesInterface $itemValue,
        ProductVariantInterface $productVariant,
        array $variants
    ): ProductVariantFilterView {
        /** @var ProductVariantFilterView $productVariantFilterView */
        $productVariantFilterView = new ProductVariantFilterView();

        $specificationItem = $itemValue->getSpecificationItemValue()->getItem();

        if (in_array($specificationItem->getType(), [ProductVariantSpecificationItemInterface::TYPE_SELECT, ProductVariantSpecificationItemInterface::TYPE_SELECT_WITH_IMAGE])) {
            $productVariantFilterView->image = $specificationItem->getImage() ? $this->imageViewFactory->create($specificationItem->getImage()) : null;
            foreach ($specificationItem->getValues() as $value) {
                $productVariantFilterView->values[] = $this->productVariantFilterValueViewFactory->create($value);
            }
        } elseif (ProductVariantSpecificationItemInterface::TYPE_INT === $specificationItem->getType()) {
            $productVariantFilterView->min = $this->getRange('min', $variants);
            $productVariantFilterView->max = $this->getRange('max', $variants);
        }

        $productVariantFilterView->code = $specificationItem->getCode();
        $productVariantFilterView->name = $specificationItem->getName();
        $productVariantFilterView->type = $specificationItem->getType();

        return $productVariantFilterView;
    }

    private function getRange(string $type, array $variants): int
    {
        $intFilterValues = [];

        foreach ($variants as $variant) {
            /** @var ArrayCollection|ProductVariantSpecificationItemValuesInterface[] $itemValues */
            $itemValues = $variant->getSpecificationItemValues();
            foreach ($itemValues as $itemValue) {
                $specificationItem = $itemValue->getSpecificationItemValue()->getItem();

                if (ProductVariantSpecificationItemInterface::TYPE_INT === $specificationItem->getType()) {
                    $intFilterValues[] = $itemValue->getIntValue();
                }
            }
        }

        sort($intFilterValues);

        return (int) ($type == 'min' ? reset($intFilterValues) ?? 0 : end($intFilterValues) ?? 0);
    }
}
