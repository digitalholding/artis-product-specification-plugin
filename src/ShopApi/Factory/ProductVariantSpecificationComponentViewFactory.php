<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\Factory;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\Artis\Common\ShopApi\Factory\Product\ProductVariantComponentViewFactoryInterface;
use DH\Artis\Common\ShopApi\View\Product\ProductVariantView;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\ProductVariantInterface as BaseProductVariantInterface;

final class ProductVariantSpecificationComponentViewFactory implements ProductVariantComponentViewFactoryInterface
{
    private VariantSpecificationViewFactoryInterface $variantSpecificationViewFactory;

    public function __construct(
        VariantSpecificationViewFactoryInterface $variantSpecificationViewFactory
    ) {
        $this->variantSpecificationViewFactory = $variantSpecificationViewFactory;
    }

    /** @phpstan-param ProductVariantInterface $variant */
    public function addComponent(
        ProductVariantView $variantView,
        BaseProductVariantInterface $variant,
        ChannelInterface $channel,
        string $locale
    ): void {
        if (null !== $variant->getProduct()->getSpecification()) {
            $variantView->specification = $this->variantSpecificationViewFactory->create(
                $variant->getProduct()->getSpecification(),
                $variant,
                $locale
            );
        }
    }
}
