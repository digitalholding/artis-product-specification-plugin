<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\Factory;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValueInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValuesAwareInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValuesInterface;
use DH\ArtisProductSpecificationPlugin\Repository\ProductVariantSpecificationItemValueRepositoryInterface;
use DH\ArtisProductSpecificationPlugin\Repository\ProductVariantSpecificationItemValuesRepositoryInterface;
use DH\ArtisProductSpecificationPlugin\ShopApi\View\VariantSpecificationItemValueView;
use Sylius\ShopApiPlugin\Factory\ImageViewFactoryInterface;
use Webmozart\Assert\Assert;

class VariantSpecificationItemValueViewFactory implements VariantSpecificationItemValueViewFactoryInterface
{
    /** @var ImageViewFactoryInterface */
    private $imageViewFactory;

    /** @var ProductVariantSpecificationItemValuesRepositoryInterface */
    private $productVariantSpecificationItemValuesRepository;

    /** @var ProductVariantSpecificationItemValueRepositoryInterface */
    private $productVariantSpecificationItemValueRepository;

    public function __construct(
        ImageViewFactoryInterface $imageViewFactory,
        ProductVariantSpecificationItemValuesRepositoryInterface $productVariantSpecificationItemValuesRepository,
        ProductVariantSpecificationItemValueRepositoryInterface $productVariantSpecificationItemValueRepository
    ) {
        $this->imageViewFactory = $imageViewFactory;
        $this->productVariantSpecificationItemValuesRepository = $productVariantSpecificationItemValuesRepository;
        $this->productVariantSpecificationItemValueRepository = $productVariantSpecificationItemValueRepository;
    }

    public function create(
        ProductVariantInterface $variant,
        ProductVariantSpecificationInterface $variantSpecification,
        ProductVariantSpecificationItemInterface $variantSpecificationItem,
        string $locale
    ): VariantSpecificationItemValueView {
        /** @var VariantSpecificationItemValueView $variantSpecificationItemValueView */
        $variantSpecificationItemValueView = new VariantSpecificationItemValueView();

        Assert::isInstanceOf($variant, ProductVariantSpecificationItemValuesAwareInterface::class);

        /** @var ProductVariantSpecificationItemValuesInterface $variantSpecificationItemValues */
        foreach ($variant->getSpecificationItemValues() as $variantSpecificationItemValues) {
            $variantSpecificationItemValue = $variantSpecificationItemValues->getSpecificationItemValue();
            $itemValue = $this->productVariantSpecificationItemValuesRepository->findByProductVariantAndSpecificationItemValue($variant, $variantSpecificationItemValue);

            if ($variantSpecificationItemValue->getItem()->getId() === $variantSpecificationItem->getId() && null !== $itemValue) {
                $this->addSpecificationItemValue($variantSpecificationItemValueView, $variantSpecificationItem, $variantSpecificationItemValue, $itemValue);
            }
        }

        return $variantSpecificationItemValueView;
    }

    private function getSpecificationItemValueByType(string $type, ProductVariantSpecificationItemValuesInterface $value)
    {
        if (ProductVariantSpecificationItemInterface::TYPE_INT === $type) {
            return $value->getIntValue();
        }
        if (ProductVariantSpecificationItemInterface::TYPE_BOOLEAN === $type) {
            return $value->isBooleanValue();
        }
        if (ProductVariantSpecificationItemInterface::TYPE_STRING === $type) {
            return $value->getValue();
        }
        if (
            ProductVariantSpecificationItemInterface::TYPE_SELECT === $type ||
            ProductVariantSpecificationItemInterface::TYPE_SELECT_WITH_IMAGE === $type) {
            /** @var ProductVariantSpecificationItemValueInterface $itemValue */
            $itemValue = $this->productVariantSpecificationItemValueRepository->findOneBy(['code' => $value->getValue()]);

            return $itemValue->getValue();
        }

        return null;
    }

    private function addSpecificationItemValue(
        VariantSpecificationItemValueView $variantSpecificationItemValueView,
        ProductVariantSpecificationItemInterface $variantSpecificationItem,
        ProductVariantSpecificationItemValueInterface $variantSpecificationItemValue,
        ProductVariantSpecificationItemValuesInterface $itemValue
    ): void {
        $value = $this->getSpecificationItemValueByType($variantSpecificationItem->getType(), $itemValue);
        $itemValueCode = '';

        if (ProductVariantSpecificationItemInterface::TYPE_INT === $variantSpecificationItem->getType() ||
            ProductVariantSpecificationItemInterface::TYPE_BOOLEAN === $variantSpecificationItem->getType() ||
            ProductVariantSpecificationItemInterface::TYPE_STRING === $variantSpecificationItem->getType()
        ) {
            $itemValueCode = $itemValue->getSpecificationItemValueCode();
        } elseif (
            ProductVariantSpecificationItemInterface::TYPE_SELECT === $variantSpecificationItem->getType() ||
            ProductVariantSpecificationItemInterface::TYPE_SELECT_WITH_IMAGE === $variantSpecificationItem->getType()
        ) {
            $itemValueCode = $itemValue->getValue();
        }

        if ($itemValueCode === $variantSpecificationItemValue->getCode()) {
            $variantSpecificationItemValueView->code = $variantSpecificationItemValue->getCode();
            $variantSpecificationItemValueView->value = $value;

            $this->addSpecificationItemValueImage($variantSpecificationItemValueView, $variantSpecificationItemValue);
        }
    }

    private function addSpecificationItemValueImage(VariantSpecificationItemValueView $variantSpecificationItemValueView, ProductVariantSpecificationItemValueInterface $variantSpecificationItemValue): void
    {
        if (null !== $variantSpecificationItemValue->getImage()) {
            $variantSpecificationItemValueView->image = $this->imageViewFactory->create($variantSpecificationItemValue->getImage());
        }
    }
}
