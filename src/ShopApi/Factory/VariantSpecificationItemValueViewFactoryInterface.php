<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\Factory;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use DH\ArtisProductSpecificationPlugin\ShopApi\View\VariantSpecificationItemValueView;

interface VariantSpecificationItemValueViewFactoryInterface
{
    public function create(
        ProductVariantInterface $variant,
        ProductVariantSpecificationInterface $variantSpecification,
        ProductVariantSpecificationItemInterface $variantSpecificationItem,
        string $locale
    ): VariantSpecificationItemValueView;
}
