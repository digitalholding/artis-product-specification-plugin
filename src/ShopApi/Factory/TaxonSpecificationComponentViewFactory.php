<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\Factory;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\Artis\Common\Repository\Product\ProductVariantRepositoryInterface;
use DH\Artis\Common\ShopApi\Factory\Taxon\TaxonComponentViewFactoryInterface;
use DH\Artis\Common\ShopApi\View\Taxon\TaxonView;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValuesInterface;
use DH\ArtisProductSpecificationPlugin\Repository\ProductVariantSpecificationItemConfigurationRepositoryInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Core\Model\TaxonInterface;

final class TaxonSpecificationComponentViewFactory implements TaxonComponentViewFactoryInterface
{
    /** @var ProductVariantSpecificationItemConfigurationRepositoryInterface */
    private $variantSpecificationItemConfigurationRepository;

    /** @var ProductVariantRepositoryInterface */
    private $productVariantRepository;

    /** @var ProductVariantFilterViewFactoryInterface */
    private $productVariantFilterViewFactory;

    public function __construct(
        ProductVariantSpecificationItemConfigurationRepositoryInterface $variantSpecificationItemConfigurationRepository,
        ProductVariantRepositoryInterface $productVariantRepository,
        ProductVariantFilterViewFactoryInterface $productVariantFilterViewFactory
    ) {
        $this->variantSpecificationItemConfigurationRepository = $variantSpecificationItemConfigurationRepository;
        $this->productVariantRepository = $productVariantRepository;
        $this->productVariantFilterViewFactory = $productVariantFilterViewFactory;
    }

    /** @phpstan-param TaxonInterface $taxon */
    public function addComponent(
        TaxonView $taxonView,
        TaxonInterface $taxon,
        string $locale
    ): void {
        /** @var ArrayCollection|ProductVariantInterface[] $variants */
        $variants = $this->productVariantRepository->findByProductTaxonCode($taxon->getCode());

        $filterCodes = [];

        foreach ($variants as $variant) {
            /** @var ArrayCollection|ProductVariantSpecificationItemValuesInterface[] $itemValues */
            $itemValues = $variant->getSpecificationItemValues();
            foreach ($itemValues as $itemValue) {
                $specificationItem = $itemValue->getSpecificationItemValue()->getItem();
                $specificationItemConfiguration = $this->variantSpecificationItemConfigurationRepository
                    ->findBySpecificationAndSpecificationItem($variant->getProduct()->getSpecification(), $specificationItem);

                $code = $specificationItem->getCode();

                if (!in_array($code, $filterCodes) && $specificationItemConfiguration->isUsedAsFilter()) {
                    $filterCodes[] = $code;

                    $taxonView->filters[] = $this->productVariantFilterViewFactory->create($itemValue, $variant, $variants);
                }
            }
        }
    }
}
