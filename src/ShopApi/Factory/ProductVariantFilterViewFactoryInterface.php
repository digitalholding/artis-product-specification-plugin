<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\Factory;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValuesInterface;
use DH\ArtisProductSpecificationPlugin\ShopApi\View\ProductVariantFilterView;

interface ProductVariantFilterViewFactoryInterface
{
    public function create(
        ProductVariantSpecificationItemValuesInterface $itemValue,
        ProductVariantInterface $productVariant,
        array $variants
    ): ProductVariantFilterView;
}
