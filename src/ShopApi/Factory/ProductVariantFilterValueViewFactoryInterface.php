<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\Factory;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValueInterface;
use DH\ArtisProductSpecificationPlugin\ShopApi\View\ProductVariantFilterValueView;

interface ProductVariantFilterValueViewFactoryInterface
{
    public function create(ProductVariantSpecificationItemValueInterface $itemValue): ProductVariantFilterValueView;
}
