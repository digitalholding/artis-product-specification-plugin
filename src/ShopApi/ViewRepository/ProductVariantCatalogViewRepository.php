<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\ViewRepository;

use DH\Artis\Common\Entity\Channel\ChannelPricing;
use DH\Artis\Common\Entity\Order\OrderItem;
use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\Artis\Common\Repository\Product\ProductVariantRepositoryInterface;
use DH\Artis\Common\ShopApi\Doctrine\Filters\ProductVariantFilter;
use DH\Artis\Common\ShopApi\ViewRepository\Product\ProductVariantCatalogViewRepositoryInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValues;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sylius\Component\Channel\Repository\ChannelRepositoryInterface;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\ShopApiPlugin\Factory\Product\PageViewFactoryInterface;
use Sylius\ShopApiPlugin\Factory\Product\ProductVariantViewFactoryInterface;
use Sylius\ShopApiPlugin\Model\PaginatorDetails;
use Sylius\ShopApiPlugin\Provider\SupportedLocaleProviderInterface;
use Sylius\ShopApiPlugin\View\Product\PageView;
use Webmozart\Assert\Assert;

final class ProductVariantCatalogViewRepository implements ProductVariantCatalogViewRepositoryInterface
{
    private ChannelRepositoryInterface $channelRepository;

    private ProductVariantRepositoryInterface $productVariantRepository;

    private ProductVariantViewFactoryInterface $productVariantViewFactory;

    private SupportedLocaleProviderInterface $supportedLocaleProvider;

    private PageViewFactoryInterface $pageViewFactory;

    /** @var ProductVariantFilter */
    private $filter;

    public function __construct(
        ChannelRepositoryInterface $channelRepository,
        ProductVariantRepositoryInterface $productVariantRepository,
        ProductVariantViewFactoryInterface $productVariantViewFactory,
        SupportedLocaleProviderInterface $supportedLocaleProvider,
        PageViewFactoryInterface $pageViewFactory,
        ProductVariantFilter $filter
    ) {
        $this->channelRepository = $channelRepository;
        $this->productVariantRepository = $productVariantRepository;
        $this->productVariantViewFactory = $productVariantViewFactory;
        $this->supportedLocaleProvider = $supportedLocaleProvider;
        $this->pageViewFactory = $pageViewFactory;
        $this->filter = $filter;
    }

    public function getVariantsByFilter(string $channelCode, PaginatorDetails $paginatorDetails, ?string $localeCode): PageView
    {
        $channel = $this->getChannel($channelCode);
        $localeCode = $this->supportedLocaleProvider->provide($localeCode, $channel);

        return $this->findEnabled($channel, $paginatorDetails, $localeCode);
    }

    private function getChannel(string $channelCode): ChannelInterface
    {
        /** @var ChannelInterface $channel */
        $channel = $this->channelRepository->findOneByCode($channelCode);

        Assert::notNull($channel, sprintf('Channel with code %s has not been found.', $channelCode));

        return $channel;
    }

    private function findEnabled(ChannelInterface $channel, PaginatorDetails $paginatorDetails, string $localeCode): PageView
    {
        //intermediate list of variant candidates
        $candidateIds = null;

        if (isset($paginatorDetails->parameters()['filters'])) {
            $filters = $paginatorDetails->parameters()['filters'];

            //if category filter is set
            if (isset($filters['category'])) {
                $candidateIds = [];
                $results = $this->createFilterQuery($channel, $localeCode, $filters, 'category')->getQuery()->getResult();

                /** @var ProductVariantInterface $variant */
                foreach ($results as $variant) {
                    $candidateIds[] = $variant->getId();
                }

                unset($filters['category']);
            }

            //if price filter is set
            if (($candidateIds === null || !empty($candidateIds)) && isset($filters['price'])) {
                $filterQueryBuilder = $this->createFilterQuery($channel, $localeCode, $filters, 'price');

                //limit to previously acquired ids
                if ($candidateIds !== null) {
                    $filterQueryBuilder->andWhere($filterQueryBuilder->expr()->in('cp.productVariant', $candidateIds));
                }

                $ids = [];
                $results = $filterQueryBuilder->getQuery()->getResult();

                /** @var ChannelPricing $channelPricing */
                foreach ($results as $channelPricing) {
                    /** @var ProductVariantInterface $variant */
                    $variant = $channelPricing->getProductVariant();
                    $ids[] = $variant->getId();
                }

                //AND we intersect values (technically we do not need the check here: just in case of any filters added before)
                $candidateIds = empty($candidateIds) ? $ids : array_intersect($candidateIds, $ids);
                unset($filters['price']);
            }

            $specFilterIds = [];

            if (($candidateIds === null || !empty($candidateIds)) && !empty($filters)) {
                foreach ($filters as $filter) {
                    $filterQueryBuilder = $this->createFilterQuery($channel, $localeCode, $filter, null);
                    $filterKey = key($filter);
                    $filterValue = $filter[$filterKey];

                    //now the problematic part: filters between types work as ANDs but within same type as ORs
                    if (!isset($specFilterIds[$filterKey])) {
                        $specFilterIds[$filterKey] = [];
                    }

                    $this->createFilterQueryClausesAndParameters($filterQueryBuilder, $filterValue);

                    //limit to previously acquired ids
                    if ($candidateIds !== null) {
                        $filterQueryBuilder->andWhere($filterQueryBuilder->expr()->in('sq.productVariant', $candidateIds));
                    }

                    $ids = [];
                    $results = $filterQueryBuilder->getQuery()->getResult();

                    /** @var ProductVariantSpecificationItemValues */
                    foreach ($results as $specificationItemValues) {
                        $ids[] = $specificationItemValues->getProductVariant()->getId();
                    }

                    //add results: acting as OR
                    $specFilterIds[$filterKey] = array_merge($specFilterIds[$filterKey], $ids);
                }
            }

            //now do a big AND over all filters
            foreach ($specFilterIds as $filterIds) {
                if ($candidateIds === null) {
                    //there were no previous filters
                    $candidateIds = $filterIds;
                } else {
                    $candidateIds = array_intersect($candidateIds, $filterIds);
                }
            }

            if ($candidateIds !== null && empty($candidateIds)) {
                //there were filters which resulted in empty queries
                //Temporary hack to return empty table
                $queryBuilder = $this->productVariantRepository->createQueryBuilderEnabledByChannel($channel, $localeCode);
                $queryBuilder->andWhere('o.id IS NULL');

                return $this->createPageView($queryBuilder, $paginatorDetails, $channel, $localeCode);
            }
        }

        $sorting = ['price' => 'desc'];

        if (array_key_exists('sorting', $paginatorDetails->parameters())) {
            $sorting = $paginatorDetails->parameters()['sorting'];
        }

        $queryBuilder = $this->getSortQueryBuilder($channel, $localeCode, $sorting);

        if (array_key_exists('search', $paginatorDetails->parameters())) {
            $searchPhrase = $paginatorDetails->parameters()['search'];

            $this->filter->filterProperty('search', $searchPhrase, $queryBuilder);
        }

        if (!empty($candidateIds)) {
            $queryBuilder->andWhere($queryBuilder->expr()->in('o.id', $candidateIds));
        }

        $pageView = $this->createPageView($queryBuilder, $paginatorDetails, $channel, $localeCode);

        return $pageView;
    }

    private function createPageView(
        QueryBuilder $queryBuilder,
        PaginatorDetails $paginatorDetails,
        ChannelInterface $channel,
        string $localeCode
    ): PageView {
        $pagerfanta = new Pagerfanta(new QueryAdapter($queryBuilder));

        $pagerfanta->setMaxPerPage($paginatorDetails->limit());
        $pagerfanta->setCurrentPage($paginatorDetails->page());

        $pageView = $this->pageViewFactory->create($pagerfanta, $paginatorDetails->route(), $paginatorDetails->parameters());

        foreach ($pagerfanta->getCurrentPageResults() as $currentPageResult) {
            $pageView->items[] = $this->productVariantViewFactory->create($currentPageResult, $channel, $localeCode);
        }

        return $pageView;
    }

    private function getSortQueryBuilder(ChannelInterface $channel, string $localeCode, ?array $sorting): QueryBuilder
    {
        if (isset($sorting['name'])) {
            $sortingValue = $sorting['name'];

            return $this->productVariantRepository->createQueryBuilderEnabledByChannel($channel, $localeCode)
                ->andWhere('o.archivedAt IS NULL')
                ->orderBy('translation.name', $sortingValue);
        }
        if (isset($sorting['price'])) {
            $sortingValue = $sorting['price'];

            return $this->productVariantRepository->createQueryBuilderEnabledByChannel($channel, $localeCode)
                ->innerJoin('o.channelPricings', 'channelPricing')
                ->andWhere('o.archivedAt IS NULL')
                ->orderBy('channelPricing.price', $sortingValue);
        }
        if (isset($sorting['order'])) {
            return $this->productVariantRepository->createQueryBuilderEnabledByChannel($channel, $localeCode)
                ->addSelect('(select sum(z.quantity) from ' . OrderItem::class . ' z where z.variant = o.id) as HIDDEN orderSum')
                ->andWhere('o.archivedAt IS NULL')
                ->orderBy('orderSum', 'desc');
        }

        return $this->productVariantRepository->createQueryBuilderEnabledByChannel($channel, $localeCode)
            ->andWhere('o.archivedAt IS NULL');
    }

    private function createFilterQuery(ChannelInterface $channel, string $localeCode, ?array $filters, ?string $type): QueryBuilder
    {
        if (null === $filters) {
            return $this->productVariantRepository->createQueryBuilderEnabledByChannel($channel, $localeCode);
        }

        switch ($type) {
            case 'category':
                $filterValue = $filters['category'];

                return $this->productVariantRepository->createQueryBuilderEnabledByChannel($channel, $localeCode)
                    ->innerJoin('product.productTaxons', 'productTaxons')
                    ->innerJoin('productTaxons.taxon', 'taxon')
                    ->andWhere('taxon.code = :code')
                    ->setParameter('code', $filterValue['equal']);

            case 'price':
                $filterValue = $filters['price'];
                $filterQueryBuilder = $this->productVariantRepository->createQueryBuilderEnabledByChannel($channel, $localeCode);

                $filterQueryBuilder->select('cp')
                    ->from(ChannelPricing::class, 'cp')
                    ->andWhere('cp.price >= :min_price')
                    ->andWhere('cp.price <= :max_price')
                    ->setParameter('min_price', $filterValue['gte'])
                    ->setParameter('max_price', $filterValue['lte']);

                return $filterQueryBuilder;
            default:
                $filterQueryBuilder = $this->productVariantRepository->createQueryBuilderEnabledByChannel($channel, $localeCode);
                $filterKey = key($filters);

                return $filterQueryBuilder->select('sq')
                    ->from(ProductVariantSpecificationItemValues::class, 'sq')
                    ->andWhere('sq.specificationItemValueCode = :code')
                    ->setParameter('code', $filterKey);
        }
    }

    private function createFilterQueryClausesAndParameters(QueryBuilder $filterQueryBuilder, array $filterValue): void
    {
        if (array_key_exists('equal', $filterValue)) {
            if (in_array($filterValue['equal'], ['true', 'false'])) { //boolean filter
                $filterQueryBuilder->andWhere('sq.booleanValue = :value')
                    ->setParameter('value', $filterValue['equal'] === 'true');
            } elseif (is_numeric($filterValue['equal'])) { //numeric filter
                $filterQueryBuilder->andWhere('sq.intValue = :value')
                    ->setParameter('value', $filterValue['equal']);
            } else { //string filter
                $filterQueryBuilder->andWhere('sq.value = :value')
                    ->setParameter('value', $filterValue['equal']);
            }
        } elseif (array_key_exists('gte', $filterValue) && array_key_exists('lte', $filterValue)) {
            if (is_numeric($filterValue['gte']) && is_numeric($filterValue['lte'])) { //numeric filter
                $filterQueryBuilder
                    ->andWhere('sq.intValue >= :min_value')
                    ->andWhere('sq.intValue <= :max_value')
                    ->setParameter('min_value', $filterValue['gte'])
                    ->setParameter('max_value', $filterValue['lte']);
            }
        }
    }
}
