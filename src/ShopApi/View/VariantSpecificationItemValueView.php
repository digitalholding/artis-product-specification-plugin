<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\View;

use Sylius\ShopApiPlugin\View\ImageView;

class VariantSpecificationItemValueView
{
    /** @var string */
    public $code;

    /** @var string */
    public $value;

    /** @var ImageView[] */
    public $image = [];
}
