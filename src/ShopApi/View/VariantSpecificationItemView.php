<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\View;

use Sylius\ShopApiPlugin\View\ImageView;

class VariantSpecificationItemView
{
    /** @var string */
    public $name;

    /** @var string */
    public $code;

    /** @var VariantSpecificationItemValueView */
    public $value;

    /** @var ImageView */
    public $image;

    /** @var bool */
    public $visibleInDescription;

    /** @var bool */
    public $visibleInProductList;

    /** @var bool */
    public $visibleInSpecification;

    /** @var bool */
    public $usedAsFilter;

    public function __construct()
    {
        $this->image = new ImageView();
    }
}
