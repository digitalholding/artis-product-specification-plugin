<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\View;

use Sylius\ShopApiPlugin\View\ImageView;

class ProductVariantFilterValueView
{
    /** @var string */
    public $name;

    /** @var string */
    public $code;

    /** @var ImageView|null */
    public $image;
}
