<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\View;

trait ProductVariantSpecificationViewAwareTrait
{
    /** @var array|VariantSpecificationView[] */
    public $specification = [];
}
