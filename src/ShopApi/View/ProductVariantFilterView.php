<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\View;

use Sylius\ShopApiPlugin\View\ImageView;

class ProductVariantFilterView
{
    /** @var string */
    public $code;

    /** @var string */
    public $name;

    /** @var string */
    public $type;

    /** @var array */
    public $values;

    /** @var int */
    public $min;

    /** @var int */
    public $max;

    /** @var ImageView|null */
    public $image;
}
