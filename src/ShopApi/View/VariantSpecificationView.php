<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\View;

use Sylius\ShopApiPlugin\View\ImageView;

class VariantSpecificationView
{
    /** @var string */
    public $name;

    /** @var string */
    public $code;

    /** @var string */
    public $description;

    /** @var ImageView[] */
    public $images = [];

    /** @var array */
    public $items = [];
}
