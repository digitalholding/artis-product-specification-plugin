<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\ShopApi\View;

trait TaxonSpecificationViewAwareTrait
{
    /** @var array|ProductVariantFilterView[] */
    public $filters;
}
