<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ImageAwareInterface;
use Sylius\Component\Resource\Model\CodeAwareInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Sylius\Component\Resource\Model\TranslationInterface;

interface ProductVariantSpecificationItemInterface extends
    ResourceInterface,
    TimestampableInterface,
    TranslatableInterface,
    ImageAwareInterface,
    CodeAwareInterface
{
    public const TYPE_INT = 'INT';

    public const TYPE_SELECT = 'SELECT';

    public const TYPE_SELECT_WITH_IMAGE = 'SELECT + IMAGE';

    public const TYPE_BOOLEAN = 'BOOLEAN';

    public const TYPE_STRING = 'STRING';

    public static function getAllTypes(): array;

    public function getTranslation(?string $locale = null): TranslationInterface;

    public function getName(): ?string;

    public function setName(?string $name): ProductVariantSpecificationItem;

    public function getValues(): ?Collection;

    public function addValue(ProductVariantSpecificationItemValueInterface $value): void;

    public function removeValue(ProductVariantSpecificationItemValueInterface $value): void;

    public function hasValue(ProductVariantSpecificationItemValueInterface $value): bool;

    public function getType(): ?string;

    public function setType(string $type): ProductVariantSpecificationItem;

    public function getConfigurations(): Collection;

    public function hasConfiguration(ProductVariantSpecificationItemConfigurationInterface $configuration): bool;

    public function addConfiguration(ProductVariantSpecificationItemConfigurationInterface $configuration): ProductVariantSpecificationItem;

    public function removeConfiguration(ProductVariantSpecificationItemConfigurationInterface $configuration): ProductVariantSpecificationItem;
}
