<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

interface ProductSpecificationAwareInterface
{
    public function getSpecification(): ?ProductVariantSpecificationInterface;

    public function setSpecification(ProductVariantSpecificationInterface $specification): self;
}
