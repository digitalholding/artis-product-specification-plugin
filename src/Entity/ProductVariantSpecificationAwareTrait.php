<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Doctrine\ORM\Mapping as ORM;

trait ProductVariantSpecificationAwareTrait
{
    /**
     * @ORM\OneToOne(
     *     targetEntity="DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface",
     *     inversedBy="variant"
     * )
     * @ORM\JoinColumn(name="specification_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $specification;

    public function getSpecification(): ?ProductVariantSpecificationInterface
    {
        return $this->specification;
    }

    public function setSpecification(ProductVariantSpecificationInterface $specification): self
    {
        $this->specification = $specification;

        return $this;
    }
}
