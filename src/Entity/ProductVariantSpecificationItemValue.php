<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class ProductVariantSpecificationItemValue implements ProductVariantSpecificationItemValueInterface
{
    use TimestampableTrait;

    /** @var int */
    protected $id;

    /**
     * @var Collection|ProductVariantSpecificationItemValuesInterface[]
     *
     * @psalm-var Collection<array-key, ProductVariantSpecificationItemValuesInterface>
     */
    protected $variantSpecificationItemValues;

    /** @var ProductVariantSpecificationItemInterface */
    protected $item;

    /** @var string */
    protected $value;

    /** @var string */
    protected $code;

    /** @var ImageInterface */
    protected $image;

    public function __construct()
    {
        $this->variantSpecificationItemValues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItem(): ?ProductVariantSpecificationItemInterface
    {
        return $this->item;
    }

    public function setItem(?ProductVariantSpecificationItemInterface $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getVariantSpecificationItemValues(): Collection
    {
        return $this->variantSpecificationItemValues;
    }

    public function hasVariantSpecificationItemValue(ProductVariantSpecificationItemValuesInterface $variantSpecificationItemValue): bool
    {
        return $this->variantSpecificationItemValues->contains($variantSpecificationItemValue);
    }

    public function addVariantSpecificationItemValue(ProductVariantSpecificationItemValuesInterface $variantSpecificationItemValue): self
    {
        if (!$this->hasVariantSpecificationItemValue($variantSpecificationItemValue)) {
            $variantSpecificationItemValue->setSpecificationItemValue($this);
            $this->variantSpecificationItemValues->add($variantSpecificationItemValue);
        }

        return $this;
    }

    public function removeVariantSpecificationItemValue(ProductVariantSpecificationItemValuesInterface $variantSpecificationItemValue): self
    {
        if ($this->hasVariantSpecificationItemValue($variantSpecificationItemValue)) {
            $this->variantSpecificationItemValues->removeElement($variantSpecificationItemValue);
            $variantSpecificationItemValue->setSpecificationItemValue(null);
        }

        return $this;
    }

    public function getImage(): ?ImageInterface
    {
        return $this->image;
    }

    public function setImage(?ImageInterface $image): void
    {
        $image->setOwner($this);
        $this->image = $image;
    }
}
