<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ImageInterface;

interface ProductVariantSpecificationImagesAwareInterface
{
    public function getVariantSpecificationImages(): Collection;

    public function hasVariantSpecificationImage(ImageInterface $image): bool;

    public function getVariantSpecificationImagesByType(string $type): Collection;

    public function hasVariantSpecificationImages(): bool;

    public function addVariantSpecificationImage(ImageInterface $image): void;

    public function removeVariantSpecificationImage(ImageInterface $image): void;
}
