<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

interface ProductVariantSpecificationItemConfigurationInterface extends ResourceInterface
{
    public function getSpecificationItem(): ?ProductVariantSpecificationItemInterface;

    public function setSpecificationItem(?ProductVariantSpecificationItemInterface $specificationItem): ProductVariantSpecificationItemConfiguration;

    public function getSpecification(): ?ProductVariantSpecificationInterface;

    public function setSpecification(?ProductVariantSpecificationInterface $specification): ProductVariantSpecificationItemConfiguration;

    public function isVisibleInSpecification(): bool;

    public function setVisibleInSpecification(bool $visibleInSpecification): ProductVariantSpecificationItemConfiguration;

    public function isUsedAsFilter(): bool;

    public function setUsedAsFilter(bool $usedAsFilter): ProductVariantSpecificationItemConfiguration;

    public function getVisibleInDescription(): bool;

    public function setVisibleInDescription(bool $visibleInDescription): ProductVariantSpecificationItemConfiguration;

    public function getVisibleInProductList(): bool;

    public function setVisibleInProductList(bool $visibleInProductList): ProductVariantSpecificationItemConfiguration;

    public function getSpecificationItemCode(): ?string;

    public function setSpecificationItemCode(?string $specificationCode): ProductVariantSpecificationItemConfiguration;
}
