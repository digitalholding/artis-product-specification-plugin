<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Doctrine\ORM\Mapping as ORM;

trait ProductVariantSpecificationItemImageAwareTrait
{
    /**
     * @ORM\OneToOne(
     *     targetEntity="DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemImage",
     *     inversedBy="image",
     *     cascade={"persist"}
     *     )
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $owner;

    public function getOwner(): ?ProductVariantSpecificationItemImage
    {
        return $this->owner;
    }

    public function setOwner(ProductVariantSpecificationItemImage $image): self
    {
        $image->setOwner($this);
        $this->owner = $image;

        return $this;
    }
}
