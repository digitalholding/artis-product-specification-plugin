<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\ImageInterface;

trait ProductVariantSpecificationImagesAwareTrait
{
    /**
     * @ORM\OneToMany(
     *     targetEntity="DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationImageInterface",
     *     mappedBy="owner",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     * @ORM\JoinColumn(
     *     onDelete="CASCADE",
     *     nullable=false
     * )
     */
    protected $variantSpecificationImages;

    public function initializeVariantSpecificationImageCollection(): void
    {
        $this->variantSpecificationImages = new ArrayCollection();
    }

    public function getVariantSpecificationImages(): Collection
    {
        return $this->variantSpecificationImages;
    }

    public function hasVariantSpecificationImage(ImageInterface $image): bool
    {
        return $this->variantSpecificationImages->contains($image);
    }

    public function getVariantSpecificationImagesByType(string $type): Collection
    {
        return $this->variantSpecificationImages->filter(function (ImageInterface $image) use ($type): bool {
            return $type === $image->getType();
        });
    }

    public function hasVariantSpecificationImages(): bool
    {
        return !$this->variantSpecificationImages->isEmpty();
    }

    public function addVariantSpecificationImage(ImageInterface $image): void
    {
        $image->setOwner($this);
        $this->variantSpecificationImages->add($image);
    }

    public function removeVariantSpecificationImage(ImageInterface $image): void
    {
        if ($this->hasVariantSpecificationImage($image)) {
            $image->setOwner(null);
            $this->variantSpecificationImages->removeElement($image);
        }
    }
}
