<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity\Translation;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslationInterface;

interface ProductVariantSpecificationItemTranslationInterface extends ResourceInterface, TranslationInterface
{
    public function getName(): ?string;

    public function setName(?string $name): ProductVariantSpecificationItemTranslation;
}
