<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity\Translation;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslationInterface;

interface ProductVariantSpecificationTranslationInterface extends ResourceInterface, TranslationInterface
{
    public function getName(): ?string;

    public function setName(?string $name): ProductVariantSpecificationTranslation;

    public function getDescription(): ?string;

    public function setDescription(?string $description): ProductVariantSpecificationTranslation;
}
