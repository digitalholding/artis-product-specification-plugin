<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity\Translation;

use Sylius\Component\Resource\Model\AbstractTranslation;

class ProductVariantSpecificationItemTranslation extends AbstractTranslation implements ProductVariantSpecificationItemTranslationInterface
{
    /** @var int */
    protected $id;

    /** @var string */
    protected $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
