<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity\Translation;

use Sylius\Component\Resource\Model\AbstractTranslation;

class ProductVariantSpecificationTranslation extends AbstractTranslation implements ProductVariantSpecificationTranslationInterface
{
    /** @var int */
    protected $id;

    /** @var string|null */
    protected $name;

    /** @var string|null */
    protected $description;

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
