<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

interface ProductVariantSpecificationItemValuesInterface extends ResourceInterface
{
    public function getSpecificationItemValue(): ?ProductVariantSpecificationItemValueInterface;

    public function setSpecificationItemValue(?ProductVariantSpecificationItemValueInterface $specificationItemValue): self;

    public function getProductVariant(): ?ProductVariantInterface;

    public function setProductVariant(ProductVariantInterface $productVariant): self;

    public function getValue(): ?string;

    public function setValue(string $value): self;

    public function isBooleanValue(): bool;

    public function setBooleanValue(bool $booleanValue);

    public function getIntValue(): ?int;

    public function setIntValue(?int $intValue): self;

    public function getSpecificationItemValueCode(): ?string;

    public function setSpecificationItemValueCode(string $specificationItemValueCode): self;
}
