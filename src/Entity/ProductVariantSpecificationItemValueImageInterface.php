<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Sylius\Component\Core\Model\ImageInterface;

interface ProductVariantSpecificationItemValueImageInterface extends ImageInterface
{
}
