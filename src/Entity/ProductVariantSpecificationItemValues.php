<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use DH\Artis\Common\Entity\Product\ProductVariantInterface;

class ProductVariantSpecificationItemValues implements ProductVariantSpecificationItemValuesInterface
{
    /** @var int */
    protected $id;

    /** @var string */
    protected $value = '';

    /** @var bool */
    protected $booleanValue = false;

    /** @var int */
    protected $intValue = 0;

    /** @var ProductVariantSpecificationItemValueInterface */
    protected $specificationItemValue;

    /** @var  ProductVariantInterface */
    protected $productVariant;

    /** @var  string */
    protected $specificationItemValueCode;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpecificationItemValue(): ?ProductVariantSpecificationItemValueInterface
    {
        return $this->specificationItemValue;
    }

    public function setSpecificationItemValue(?ProductVariantSpecificationItemValueInterface $specificationItemValue): self
    {
        $this->specificationItemValue = $specificationItemValue;

        return $this;
    }

    public function getProductVariant(): ?ProductVariantInterface
    {
        return $this->productVariant;
    }

    public function setProductVariant(ProductVariantInterface $productVariant): self
    {
        $this->productVariant = $productVariant;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function isBooleanValue(): bool
    {
        return $this->booleanValue;
    }

    public function setBooleanValue(bool $booleanValue)
    {
        $this->booleanValue = $booleanValue;
    }

    public function getIntValue(): ?int
    {
        return $this->intValue;
    }

    public function setIntValue(?int $intValue): self
    {
        $this->intValue = $intValue;

        return $this;
    }

    public function getSpecificationItemValueCode(): ?string
    {
        return $this->specificationItemValueCode;
    }

    public function setSpecificationItemValueCode(string $specificationItemValueCode): self
    {
        $this->specificationItemValueCode = $specificationItemValueCode;

        return $this;
    }
}
