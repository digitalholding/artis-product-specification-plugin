<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use DH\ArtisProductSpecificationPlugin\Entity\Translation\ProductVariantSpecificationItemTranslation;
use DH\ArtisProductSpecificationPlugin\Entity\Translation\ProductVariantSpecificationItemTranslationInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;
use Sylius\Component\Resource\Model\TranslatableTrait;
use Sylius\Component\Resource\Model\TranslationInterface;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;

class ProductVariantSpecificationItem implements ProductVariantSpecificationItemInterface
{
    use TimestampableTrait;
    use TranslatableTrait {
        __construct as private initializeTranslationsCollection;
        getTranslation as private doGetTranslation;
    }

    /** @var int */
    protected $id;

    /**
     * @var Collection|ProductVariantSpecificationItemConfigurationInterface[]
     *
     * @psalm-var Collection<array-key, ProductVariantSpecificationItemConfigurationInterface>
     */
    protected $configurations;

    /** @var string */
    protected $type;

    /** @var string */
    protected $code;

    /**
     * @var Collection|ProductVariantSpecificationItemValueInterface[]
     *
     * @psalm-var Collection<array-key, ProductVariantSpecificationItemValueInterface>
     */
    protected $values;

    /** @var ImageInterface */
    protected $image;

    public function __construct()
    {
        $this->initializeTranslationsCollection();

        $this->values = new ArrayCollection();
        $this->configurations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getConfigurations(): Collection
    {
        return $this->configurations;
    }

    public function hasConfiguration(ProductVariantSpecificationItemConfigurationInterface $configuration): bool
    {
        return $this->configurations->contains($configuration);
    }

    public function addConfiguration(ProductVariantSpecificationItemConfigurationInterface $configuration): self
    {
        if (!$this->hasConfiguration($configuration)) {
            $configuration->setSpecificationItem($this);
            $this->configurations->add($configuration);
        }

        return $this;
    }

    public function removeConfiguration(ProductVariantSpecificationItemConfigurationInterface $configuration): self
    {
        if ($this->hasConfiguration($configuration)) {
            $this->configurations->removeElement($configuration);
            $configuration->setSpecificationItem(null);
        }

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $allTypes = self::getAllTypes();

        if (!in_array($type, $allTypes)) {
            throw new InvalidTypeException('Invalid type. Possible types are ' . implode(', ', $allTypes));
        }

        $this->type = $type;

        return $this;
    }

    public static function getAllTypes(): array
    {
        return [
            self::TYPE_INT,
            self::TYPE_SELECT,
            self::TYPE_SELECT_WITH_IMAGE,
            self::TYPE_BOOLEAN,
            self::TYPE_STRING
        ];
    }

    public function getImage(): ?ImageInterface
    {
        return $this->image;
    }

    public function setImage(?ImageInterface $image): void
    {
        $image->setOwner($this);
        $this->image = $image;
    }

    public function getName(): ?string
    {
        return $this->getTranslation()->getName();
    }

    public function setName(?string $name): self
    {
        $this->getTranslation()->setName($name);

        return $this;
    }

    public function getValues(): ?Collection
    {
        return $this->values;
    }

    public function addValue(ProductVariantSpecificationItemValueInterface $value): void
    {
        if (!$this->hasValue($value)) {
            $value->setItem($this);
            $this->values->add($value);
        }
    }

    public function hasValue(ProductVariantSpecificationItemValueInterface $value): bool
    {
        return $this->values->contains($value);
    }

    public function removeValue(ProductVariantSpecificationItemValueInterface $value): void
    {
        if ($this->hasValue($value)) {
            $this->values->removeElement($value);
            $value->setItem(null);
        }
    }

    public function getTranslation(?string $locale = null): TranslationInterface
    {
        /** @var ProductVariantSpecificationItemTranslationInterface $translation */
        $translation = $this->doGetTranslation($locale);

        return $translation;
    }

    protected function createTranslation(): ProductVariantSpecificationItemTranslationInterface
    {
        return new ProductVariantSpecificationItemTranslation();
    }
}
