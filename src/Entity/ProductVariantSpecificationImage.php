<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Sylius\Component\Core\Model\Image;

class ProductVariantSpecificationImage extends Image implements ProductVariantSpecificationImageInterface
{
}
