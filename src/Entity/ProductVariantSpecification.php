<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use DH\Artis\Common\Entity\Product\ProductInterface;
use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\ArtisProductSpecificationPlugin\Entity\Translation\ProductVariantSpecificationTranslation;
use DH\ArtisProductSpecificationPlugin\Entity\Translation\ProductVariantSpecificationTranslationInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Resource\Model\TimestampableTrait;
use Sylius\Component\Resource\Model\TranslatableTrait;
use Sylius\Component\Resource\Model\TranslationInterface;

class ProductVariantSpecification implements ProductVariantSpecificationInterface
{
    use TimestampableTrait;
    use TranslatableTrait {
        __construct as private initializeTranslationsCollection;
        getTranslation as private doGetTranslation;
    }

    /** @var int|null */
    protected $id;

    /** @var string */
    protected $code;

    /** @var ProductVariantInterface|null */
    protected $variant;

    /** @var ProductInterface|null */
    protected $product;

    /** @var ProductVariantSpecificationItemInterface[]|Collection */
    protected $items;

    /** @var ProductVariantSpecificationItemConfigurationInterface[]|Collection */
    protected $itemConfigurations;

    public function __construct()
    {
        $this->initializeTranslationsCollection();

        $this->items = new ArrayCollection();
        $this->itemConfigurations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return ProductVariantSpecificationTranslationInterface|TranslationInterface
     */
    public function getTranslation(?string $locale = null): TranslationInterface
    {
        /** @var ProductVariantSpecificationTranslationInterface $translation */
        $translation = $this->doGetTranslation($locale);

        return $translation;
    }

    public function getVariant(): ?ProductVariantInterface
    {
        return $this->variant;
    }

    public function setVariant(?ProductVariantInterface $variant): self
    {
        $variant->setSpecification($this);
        $this->variant = $variant;

        return $this;
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(ProductVariantSpecificationItemInterface $item): void
    {
        if (!$this->hasItem($item)) {
            $this->items->add($item);
        }
    }

    public function removeItem(ProductVariantSpecificationItemInterface $item): void
    {
        if ($this->hasItem($item)) {
            $this->items->removeElement($item);
        }
    }

    public function hasItem(ProductVariantSpecificationItemInterface $item): bool
    {
        return $this->items->contains($item);
    }

    public function getDescription(): ?string
    {
        return $this->getTranslation()->getDescription();
    }

    public function setDescription(?string $description): self
    {
        $this->getTranslation()->setDescription($description);

        return $this;
    }

    public function getName(): ?string
    {
        return $this->getTranslation()->getName();
    }

    public function setName(?string $name): self
    {
        $this->getTranslation()->setName($name);

        return $this;
    }

    public function getProduct(): ?ProductInterface
    {
        return $this->product;
    }

    public function setProduct(?ProductInterface $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getItemConfigurations(): Collection
    {
        return $this->itemConfigurations;
    }

    public function hasItemConfiguration(ProductVariantSpecificationItemConfigurationInterface $itemConfiguration): bool
    {
        return $this->itemConfigurations->contains($itemConfiguration);
    }

    public function addItemConfiguration(ProductVariantSpecificationItemConfigurationInterface $itemConfiguration): self
    {
        if (!$this->hasItemConfiguration($itemConfiguration)) {
            $itemConfiguration->setSpecification($this);
            $this->itemConfigurations->add($itemConfiguration);
        }

        return $this;
    }

    public function removeItemConfiguration(ProductVariantSpecificationItemConfigurationInterface $itemConfiguration): self
    {
        if ($this->hasItemConfiguration($itemConfiguration)) {
            $this->itemConfigurations->removeElement($itemConfiguration);
            $itemConfiguration->setSpecification(null);
        }

        return $this;
    }

    protected function createTranslation(): ProductVariantSpecificationTranslationInterface
    {
        return new ProductVariantSpecificationTranslation();
    }
}
