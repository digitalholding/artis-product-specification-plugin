<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ImageAwareInterface;
use Sylius\Component\Resource\Model\CodeAwareInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableInterface;

interface ProductVariantSpecificationItemValueInterface extends
    ResourceInterface,
    TimestampableInterface,
    ImageAwareInterface,
    CodeAwareInterface
{
    public function getItem(): ?ProductVariantSpecificationItemInterface;

    public function setItem(?ProductVariantSpecificationItemInterface $item): ProductVariantSpecificationItemValue;

    public function getValue(): ?string;

    public function setValue(string $value): ProductVariantSpecificationItemValue;

    public function getVariantSpecificationItemValues(): Collection;

    public function hasVariantSpecificationItemValue(ProductVariantSpecificationItemValuesInterface $variantSpecificationItemValue): bool;

    public function addVariantSpecificationItemValue(ProductVariantSpecificationItemValuesInterface $variantSpecificationItemValue): ProductVariantSpecificationItemValue;

    public function removeVariantSpecificationItemValue(ProductVariantSpecificationItemValuesInterface $variantSpecificationItemValue): ProductVariantSpecificationItemValue;
}
