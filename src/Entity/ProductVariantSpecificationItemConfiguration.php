<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

class ProductVariantSpecificationItemConfiguration implements ProductVariantSpecificationItemConfigurationInterface
{
    /** @var int */
    protected $id;

    /** @var bool */
    protected $visibleInDescription = false;

    /** @var bool */
    protected $visibleInProductList = false;

    /** @var bool */
    protected $visibleInSpecification = false;

    /** @var bool */
    protected $usedAsFilter = false;

    /** @var ProductVariantSpecificationInterface */
    protected $specification;

    /** @var string */
    protected $specificationItemCode;

    /** @var ProductVariantSpecificationItemInterface */
    protected $specificationItem;

    public function getId(): ?int
    {
        return $this->id();
    }

    public function isVisibleInDescription(): bool
    {
        return $this->visibleInDescription;
    }

    public function setVisibleInDescription(bool $visibleInDescription): self
    {
        $this->visibleInDescription = $visibleInDescription;

        return $this;
    }

    public function getVisibleInProductList(): bool
    {
        return $this->visibleInProductList;
    }

    public function setVisibleInProductList(bool $visibleInProductList): self
    {
        $this->visibleInProductList = $visibleInProductList;

        return $this;
    }

    public function getVisibleInSpecification(): bool
    {
        return $this->visibleInSpecification;
    }

    public function setVisibleInSpecification(bool $visibleInSpecification): self
    {
        $this->visibleInSpecification = $visibleInSpecification;

        return $this;
    }

    public function getUsedAsFilter(): bool
    {
        return $this->usedAsFilter;
    }

    public function setUsedAsFilter(bool $usedAsFilter): self
    {
        $this->usedAsFilter = $usedAsFilter;

        return $this;
    }

    public function getSpecification(): ?ProductVariantSpecificationInterface
    {
        return $this->specification;
    }

    public function setSpecification(?ProductVariantSpecificationInterface $specification): self
    {
        $this->specification = $specification;

        return $this;
    }

    public function getSpecificationItemCode(): ?string
    {
        return $this->specificationItemCode;
    }

    public function setSpecificationItemCode(?string $specificationItemCode): self
    {
        $this->specificationItemCode = $specificationItemCode;

        return $this;
    }

    public function getSpecificationItem(): ?ProductVariantSpecificationItemInterface
    {
        return $this->specificationItem;
    }

    public function setSpecificationItem(?ProductVariantSpecificationItemInterface $specificationItem): self
    {
        $this->specificationItem = $specificationItem;

        return $this;
    }

    public function isVisibleInSpecification(): bool
    {
        return $this->visibleInSpecification;
    }

    public function isUsedAsFilter(): bool
    {
        return $this->usedAsFilter;
    }

    public function getVisibleInDescription(): bool
    {
        return $this->visibleInDescription;
    }
}
