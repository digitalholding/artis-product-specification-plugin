<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Doctrine\ORM\Mapping as ORM;

trait ProductOptionTrait
{
    /** @ORM\Column(type="boolean", nullable=false, options={"default" : 0}) */
    protected $specificationParameter;

    public function getSpecificationParameter()
    {
        return $this->specificationParameter;
    }

    public function setSpecificationParameter(bool $specificationParameter): self
    {
        $this->specificationParameter = $specificationParameter;

        return $this;
    }
}
