<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

trait ProductVariantSpecificationItemValuesAwareTrait
{
    /**
     * @ORM\OneToMany(
     *     targetEntity="DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValuesInterface",
     *     mappedBy="productVariant",
     *     cascade={"all"},
     *     orphanRemoval=true,
     *     indexBy="specificationItemValueCode"
     * )
     * @ORM\JoinColumn(
     *     onDelete="CASCADE",
     *     nullable=false
     * )
     */
    protected $specificationItemValues;

    public function initializeVariantSpecificationItemValuesCollection(): void
    {
        $this->specificationItemValues = new ArrayCollection();
    }

    public function getSpecificationItemValues(): Collection
    {
        return $this->specificationItemValues;
    }

    public function addSpecificationItemValue(ProductVariantSpecificationItemValuesInterface $specificationItemValue): void
    {
        if (!$this->hasSpecificationItemValue($specificationItemValue)) {
            $specificationItemValue->setProductVariant($this);
            $this->specificationItemValues->add($specificationItemValue);
        }
    }

    public function removeSpecificationItemValue(ProductVariantSpecificationItemValuesInterface $specificationItemValue): void
    {
        if ($this->hasSpecificationItemValue($specificationItemValue)) {
            $this->specificationItemValues->removeElement($specificationItemValue);
            $specificationItemValue->setProductVariant(null);
        }
    }

    public function hasSpecificationItemValue(ProductVariantSpecificationItemValuesInterface $specificationItemValue): bool
    {
        return $this->specificationItemValues->contains($specificationItemValue);
    }
}
