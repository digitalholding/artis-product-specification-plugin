<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Doctrine\Common\Collections\Collection;

interface ProductVariantSpecificationItemValuesAwareInterface
{
    public function initializeVariantSpecificationItemValuesCollection(): void;

    public function getSpecificationItemValues(): Collection;

    public function addSpecificationItemValue(ProductVariantSpecificationItemValuesInterface $specificationItemValue): void;

    public function removeSpecificationItemValue(ProductVariantSpecificationItemValuesInterface $specificationItemValue): void;

    public function hasSpecificationItemValue(ProductVariantSpecificationItemValuesInterface $specificationItemValue): bool;
}
