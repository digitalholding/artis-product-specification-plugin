<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use DH\Artis\Common\Entity\Product\ProductInterface;
use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\ArtisProductSpecificationPlugin\Entity\Translation\ProductVariantSpecificationTranslationInterface;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Sylius\Component\Resource\Model\TranslationInterface;

interface ProductVariantSpecificationInterface extends
    ResourceInterface,
    TimestampableInterface,
    TranslatableInterface
{
    public function getCode(): ?string;

    public function setCode(?string $code): self;

    /**
     * @return ProductVariantSpecificationTranslationInterface|TranslationInterface
     */
    public function getTranslation(?string $locale = null): TranslationInterface;

    public function getVariant(): ?ProductVariantInterface;

    public function setVariant(?ProductVariantInterface $variant): self;

    public function getItems(): Collection;

    public function addItem(ProductVariantSpecificationItemInterface $item): void;

    public function removeItem(ProductVariantSpecificationItemInterface $item): void;

    public function hasItem(ProductVariantSpecificationItemInterface $item): bool;

    public function getDescription(): ?string;

    public function setDescription(?string $description): self;

    public function getName(): ?string;

    public function setName(?string $name): self;

    public function getProduct(): ?ProductInterface;

    public function setProduct(?ProductInterface $product): self;

    public function getItemConfigurations(): Collection;

    public function hasItemConfiguration(ProductVariantSpecificationItemConfigurationInterface $itemConfiguration): bool;

    public function addItemConfiguration(ProductVariantSpecificationItemConfigurationInterface $itemConfiguration): self;

    public function removeItemConfiguration(ProductVariantSpecificationItemConfigurationInterface $itemConfiguration): self;
}
