<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Entity;

use Doctrine\ORM\Mapping as ORM;

trait ProductSpecificationAwareTrait
{
    /**
     * @ORM\OneToOne(
     *     targetEntity="DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface",
     *     inversedBy="product",
     *     cascade={"persist", "remove"}
     *     )
     * @ORM\JoinColumn(name="specification_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $specification;

    public function getSpecification(): ?ProductVariantSpecificationInterface
    {
        return $this->specification;
    }

    public function setSpecification(ProductVariantSpecificationInterface $specification): self
    {
        $specification->setProduct($this);
        $this->specification = $specification;

        return $this;
    }
}
