<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Form\Extension;

use DH\Artis\Common\Entity\Product\ProductOptionValue;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValue;
use DH\ArtisProductSpecificationPlugin\Factory\ProductVariantSpecificationItemFactoryInterface;
use DH\ArtisProductSpecificationPlugin\Factory\ProductVariantSpecificationItemValueFactoryInterface;
use Sylius\Bundle\ProductBundle\Form\Type\ProductOptionType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ProductOptionTypeExtension extends AbstractTypeExtension
{
    /** @var ProductVariantSpecificationItemFactoryInterface */
    private $productSpecificationItemFactory;

    /** @var ProductVariantSpecificationItemValueFactoryInterface */
    private $productSpecificationItemValueFactory;

    /** @var RepositoryInterface */
    private $baseRepository;

    public function __construct(
        ProductVariantSpecificationItemFactoryInterface $factory,
        ProductVariantSpecificationItemValueFactoryInterface $itemValueFactory,
        RepositoryInterface $repository
    ) {
        $this->productSpecificationItemFactory = $factory;
        $this->productSpecificationItemValueFactory = $itemValueFactory;
        $this->baseRepository = $repository;
    }

    public static function getExtendedTypes(): iterable
    {
        return [ProductOptionType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('specificationParameter', CheckboxType::class, [
                'required' => false,
                'label' => 'dh_artis_product_specification_plugin.form.product_option.specificationParameter',
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                /** @var ProductOptionValue $data */
                $data = $event->getData();

                if (!empty($data->getCode()) && $data->getSpecificationParameter()) {
                    $productItem = $this->baseRepository->findOneBy(['code' => $data->getCode()]);

                    if ($productItem === null) {
                        $productItem = $this->productSpecificationItemFactory->createNewWithType('select', $data->getCode());
                        $this->baseRepository->add($productItem);
                    }

                    $productOptionValues = $event->getData()->getValues();

                    /** @var ProductOptionValue $productOptionValue */
                    foreach ($productOptionValues as $productOptionValue) {
                        $code = $productOptionValue->getCode();
                        $value = $productOptionValue->getValue();
                        $productSpecificationItemValue = $this->baseRepository->findOneBy(['code' => $code, 'value' => $value]);

                        if ($productSpecificationItemValue === null) {
                            /** @var ProductVariantSpecificationItemValue $productSpecificationItemValue */
                            $productSpecificationItemValue = $this->productSpecificationItemValueFactory->createWithItem($productItem);
                            $productSpecificationItemValue->setCode($code);
                            $productSpecificationItemValue->setValue($value);
                            $this->baseRepository->add($productSpecificationItemValue);
                        }
                    }
                }
            });
    }
}
