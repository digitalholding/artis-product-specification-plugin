<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Form\Extension;

use DH\Artis\Common\Entity\Product\ProductInterface;
use DH\Artis\Common\Entity\Product\ProductVariantInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductSpecificationAwareInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValues;
use DH\ArtisProductSpecificationPlugin\Form\Type\ProductVariantSpecificationImageType;
use DH\ArtisProductSpecificationPlugin\Form\Type\ProductVariantSpecificationItemValuesType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductVariantType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Valid;
use Webmozart\Assert\Assert;

final class ProductVariantTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('variantSpecificationImages', CollectionType::class, [
                'entry_type' => ProductVariantSpecificationImageType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => 'dh.form.product_variant_specification.images',
                'block_name' => 'entry',
                'constraints' => [new Valid()]
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            /** @var ProductVariantInterface|null $productVariant */
            $productVariant = $event->getData();
            $this->fillMissingSpecificationItems($productVariant);

            if (null === $productVariant) {
                return;
            }

            /** @var ProductInterface $product */
            $product = $productVariant->getProduct();

            Assert::isInstanceOf($product, ProductSpecificationAwareInterface::class);
            $specification = $product->getSpecification();

            if ($specification) {
                $event->getForm()->add('specificationItemValues', CollectionType::class, [
                    'entry_type' => ProductVariantSpecificationItemValuesType::class,
                    'allow_add' => false,
                    'label' => false,
                    'block_name' => 'entry',
                    'constraints' => [new Valid()]
                ]);
            }
        });
    }

    public static function getExtendedTypes(): iterable
    {
        return [ProductVariantType::class];
    }

    protected function fillMissingSpecificationItems(ProductVariantInterface $variant): void
    {
        if (!($specification = $variant->getProduct()->getSpecification())) {
            return;
        }

        $existingItems = $variant->getSpecificationItemValues();
        $existingIds = [];
        foreach ($existingItems as $item) {
            $existingIds[$item->getSpecificationItemValue()->getItem()->getId()] = true;
        }

        foreach ($specification->getItems() as $item) {
            if (!isset($existingIds[$item->getId()])) {
                $newValuesValue = new ProductVariantSpecificationItemValues();
                $newValuesValue->setProductVariant($variant)
                    ->setSpecificationItemValue($item->getValues()[0])
                    ->setSpecificationItemValueCode($item->getCode())
                ;

                $variant->getSpecificationItemValues()->add($newValuesValue);
            }
        }
    }
}
