<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Form\Extension;

use DH\Artis\Common\Entity\Product\ProductInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductSpecificationAwareInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecification;
use DH\ArtisProductSpecificationPlugin\Factory\ProductVariantSpecificationFactory;
use DH\ArtisProductSpecificationPlugin\Form\Type\ProductVariantSpecificationType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Webmozart\Assert\Assert;

final class ProductTypeExtension extends AbstractTypeExtension
{
    /** @var ProductVariantSpecificationFactory */
    private $factory;

    /** @var RepositoryInterface */
    private $repository;

    public function __construct(
        ProductVariantSpecificationFactory $factory,
        RepositoryInterface $repository
    ) {
        $this->factory = $factory;
        $this->repository = $repository;
    }

    public static function getExtendedTypes(): iterable
    {
        return [ProductType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            /** @var ProductInterface $product */
            $product = $event->getData();
            Assert::isInstanceOf($product, ProductSpecificationAwareInterface::class);

            if (!empty($product)) {
                if ($product->getSpecification() === null) {
                    /** @var ProductVariantSpecification $productVariantSpecification */
                    $productVariantSpecification = $this->factory->createWithCode();

                    $product->setSpecification($productVariantSpecification);
                    $event->setData($product);
                }
            }

            $event->getForm()->add('specification', ProductVariantSpecificationType::class, [
                'label' => false,
            ]);
        });
    }
}
