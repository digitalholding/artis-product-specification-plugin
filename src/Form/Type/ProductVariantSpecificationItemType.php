<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Form\Type;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItem;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValue;
use DH\ArtisProductSpecificationPlugin\Form\Type\Translation\ProductVariantSpecificationItemTranslationType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

final class ProductVariantSpecificationItemType extends AbstractResourceType
{
    /** @var RepositoryInterface */
    private $baseRepository;

    /** @var RepositoryInterface */
    private $repositoryItemValue;

    public function __construct(
        string $dataClass,
        RepositoryInterface $baseRepository,
        RepositoryInterface $repositoryItemValue,
        array $validationGroups = []
    ) {
        parent::__construct($dataClass, $validationGroups);

        $this->baseRepository = $baseRepository;
        $this->repositoryItemValue = $repositoryItemValue;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('code', TextType::class, [
                'label' => 'sylius.ui.code',
                'disabled' => null !== $builder->getData()->getCode(),
            ])
            ->add('translations', ResourceTranslationsType::class, [
                'label' => 'dh_artis_product_specification_plugin.form.product_variant.specification_item.translations',
                'entry_type' => ProductVariantSpecificationItemTranslationType::class,
            ])
            ->add('values', CollectionType::class, [
                'label' => false,
                'entry_type' => ProductVariantSpecificationItemValueType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'button_add_label' => 'dh_artis_product_specification_plugin.form.product_variant.specification_item.add_value',
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'artis.ui.type',
                'choices' => ProductVariantSpecificationItem::getAllTypes(),
                'choice_label' => function ($choice) {
                    return 'dh_artis_product_specification_plugin.form.product_variant.specification_item.' . $choice;
                }
            ])
            ->add('image', ProductVariantSpecificationImageType::class, [
                'label' => 'sylius.ui.image',
                'required' => false,
            ]);

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) use ($options): void {
            $specificationItem = $event->getData();
            $form = $event->getForm();

            if (!$specificationItem instanceof $this->dataClass || !$specificationItem instanceof ProductVariantSpecificationItemInterface) {
                $event->setData(null);

                return;
            }

            if ($form->isSubmitted() && $form->isValid()) {
                $specificationItemValues = !$specificationItem->getValues()->isEmpty() ?
                    $specificationItem->getValues()->toArray() : [];

                if (null !== $specificationItem->getId() || 0 !== count($specificationItemValues)) {
                    return;
                }

                $this->baseRepository->add($specificationItem);

                $value = new ProductVariantSpecificationItemValue();
                $value->setCode($specificationItem->getCode());
                $value->setValue('');
                $value->setItem($specificationItem);

                $this->repositoryItemValue->add($value);
            }
        });
    }

    public function getBlockPrefix(): string
    {
        return 'dh_product_variant_specification_item';
    }
}
