<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Form\Type;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use Sylius\Bundle\ResourceBundle\Form\Type\FixedCollectionType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ProductVariantSpecificationItemConfigurationCollectionType extends AbstractType
{
    /** @var RepositoryInterface */
    private $productVariantSpecificationItemRepository;

    public function __construct(
        RepositoryInterface $productVariantSpecificationItemRepository
    ) {
        $this->productVariantSpecificationItemRepository = $productVariantSpecificationItemRepository;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'entries' => $this->productVariantSpecificationItemRepository->findAll(),
                'entry_name' => function (ProductVariantSpecificationItemInterface $productVariantSpecificationItem) {
                    return $productVariantSpecificationItem->getCode();
                },
                'error_bubbling' => false,
            ])
            ->setRequired([
                'option',
            ]);
    }

    public function getParent(): string
    {
        return FixedCollectionType::class;
    }

    public function getBlockPrefix(): string
    {
        return 'dh_product_variant_specification_item_configuration_collection';
    }
}
