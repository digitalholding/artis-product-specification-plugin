<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Form\Type;

use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Bridge\Doctrine\Form\DataTransformer\CollectionToArrayTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ProductVariantSpecificationItemChoiceType extends AbstractType
{
    /** @var RepositoryInterface */
    private $productVariantSpecificationItemRepository;

    public function __construct(
        RepositoryInterface $productVariantSpecificationItemRepository
    ) {
        $this->productVariantSpecificationItemRepository = $productVariantSpecificationItemRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['multiple']) {
            $builder->addModelTransformer(new CollectionToArrayTransformer());
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choices' => $this->productVariantSpecificationItemRepository->findAll(),
            'choice_value' => 'code',
            'choice_label' => 'name',
            'choice_translation_domain' => false,
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }

    public function getBlockPrefix(): string
    {
        return 'dh_product_variant_specification_item_choice';
    }
}
