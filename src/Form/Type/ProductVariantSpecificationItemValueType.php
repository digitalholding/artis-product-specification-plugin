<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Form\Type;

use Sylius\Bundle\ResourceBundle\Form\EventSubscriber\AddCodeFormSubscriber;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

final class ProductVariantSpecificationItemValueType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addEventSubscriber(new AddCodeFormSubscriber())
            ->add('value', TextType::class, [
                'required' => false,
                'empty_data' => '',
                'label' => 'dh_artis_product_specification_plugin.form.product_variant.specification_item.value'
            ])
            ->add('image', ProductVariantSpecificationItemValueImageType::class, [
                'label' => false,
                'required' => false,
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'dh_product_variant_specification_item_value';
    }
}
