<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Form\Type;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItem;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValue;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValues;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemValuesInterface;
use DH\ArtisProductSpecificationPlugin\Factory\ProductVariantSpecificationItemFactory;
use DH\ArtisProductSpecificationPlugin\Factory\ProductVariantSpecificationItemValueFactory;
use DH\ArtisProductSpecificationPlugin\Repository\ProductVariantSpecificationItemValueRepository;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

final class ProductVariantSpecificationItemValuesType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {
            /** @var ProductVariantSpecificationItemValues $specificationItemValues */
            $specificationItemValues = $event->getData();
            /** @var ProductVariantSpecificationItemValue $specificationItemValue */
            $specificationItemValue = $specificationItemValues->getSpecificationItemValue();

            $specificationItem = $specificationItemValue->getItem();
            $builder = $event->getForm();

            switch ($specificationItem->getType()) {
                case ProductVariantSpecificationItemInterface::TYPE_SELECT_WITH_IMAGE:
                case ProductVariantSpecificationItemInterface::TYPE_SELECT:
                    $builder->add('specificationItemValue', EntityType::class, [
                        'class' => ProductVariantSpecificationItemValue::class,
                        'choice_label' => 'value',
                        'query_builder' => function (ProductVariantSpecificationItemValueRepository $repository) use ($specificationItem) {
                            return $repository->getQuerySpecificationItem($specificationItem->getId());
                        },
                        'constraints' => [
                            new NotBlank(['message' => 'dh.product_variant_specification_item_values.value'])
                        ],
                        'label' => $specificationItem->getName()
                    ]);

                    break;
                case ProductVariantSpecificationItemInterface::TYPE_STRING:
                    $builder
                        ->add('value', TextType::class, [
                            'label' => $specificationItem->getName(),
                            'empty_data' => '',
                            'constraints' => [
                                new NotBlank(['message' => 'dh.product_variant_specification_item_values.value'])
                            ]
                        ]);

                    break;
                case ProductVariantSpecificationItemInterface::TYPE_BOOLEAN:
                    $builder
                        ->add('booleanValue', CheckboxType::class, [
                            'label' => $specificationItem->getName()
                        ]);

                    break;
                case ProductVariantSpecificationItemInterface::TYPE_INT:
                    $builder
                        ->add('intValue', IntegerType::class, [
                            'empty_data' => '0',
                            'label' => $specificationItem->getName()
                        ]);

                    break;
                default:
                    return;
            }
        });

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event): void {
            /** @var ProductVariantSpecificationItemValues */
            $productVariantSpecificationItemValue = $event->getData();

            if (!$productVariantSpecificationItemValue instanceof $this->dataClass ||
                !$productVariantSpecificationItemValue instanceof ProductVariantSpecificationItemValuesInterface
            ) {
                $event->setData(null);

                return;
            }

            /** @var ProductVariantSpecificationItemValue $itemValue */
            $itemValue = $productVariantSpecificationItemValue->getSpecificationItemValue();
            /** @var ProductVariantSpecificationItem $productVariantSpecificationItem */
            $productVariantSpecificationItem = $itemValue->getItem();

            $productVariantSpecificationItemValue->setSpecificationItemValueCode($productVariantSpecificationItem->getCode());
            /* For `select` or `select+image` we store the code of the selection value as value's value (sounds that sounds nice!)
            so use `specification item value code` stores the code of Item (which can have selection options) while value stores
            the code. Thanks to this we can avoid additional table and complex logic especially for selections. */
            if (in_array($productVariantSpecificationItem->getType(), [
                ProductVariantSpecificationItemInterface::TYPE_SELECT,
                ProductVariantSpecificationItemInterface::TYPE_SELECT_WITH_IMAGE
            ])) {
                $productVariantSpecificationItemValue->setValue($itemValue->getCode());
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver
            ->setDefaults([
                'data_class' => ProductVariantSpecificationItemValues::class,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'dh_product_variant_specification_item_values';
    }
}
