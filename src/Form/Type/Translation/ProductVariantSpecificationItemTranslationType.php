<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Form\Type\Translation;

use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

final class ProductVariantSpecificationItemTranslationType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextareaType::class, [
                'label' => 'dh_artis_product_specification_plugin.form.product_variant.specification_item.name',
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'dh_artis_product_variant_specification_item_translation';
    }
}
