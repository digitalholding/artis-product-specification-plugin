<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Form\Type;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemConfigurationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ProductVariantSpecificationItemConfigurationType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('visibleInDescription', CheckboxType::class, [
                'label' => 'dh_artis_product_specification_plugin.form.product_variant.specification.item.visible_in_description',
                'validation_groups' => ['sylius'],
            ])
            ->add('visibleInProductList', CheckboxType::class, [
                'label' => 'dh_artis_product_specification_plugin.form.product_variant.specification.item.visible_in_product_list',
                'validation_groups' => ['sylius'],
            ])
            ->add('visibleInSpecification', CheckboxType::class, [
                'label' => 'dh_artis_product_specification_plugin.form.product_variant.specification.item.visible_in_specification',
                'validation_groups' => ['sylius'],
            ])
            ->add('usedAsFilter', CheckboxType::class, [
                'label' => 'dh_artis_product_specification_plugin.form.product_variant.specification.item.used_as_filter',
                'validation_groups' => ['sylius'],
            ]);

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) use ($options): void {
            $productVariantSpecificationItemConfiguration = $event->getData();

            if (!$productVariantSpecificationItemConfiguration instanceof $this->dataClass ||
                !$productVariantSpecificationItemConfiguration instanceof ProductVariantSpecificationItemConfigurationInterface
            ) {
                $event->setData(null);

                return;
            }

            $specification = $options['specification'];
            $specificationItem = $options['specification_item'];

            $specification->addItem($specificationItem);

            $productVariantSpecificationItemConfiguration->setSpecificationItemCode($specificationItem->getCode())
                ->setSpecification($specification)
                ->setSpecificationItem($specificationItem);

            $event->setData($productVariantSpecificationItemConfiguration);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver
            ->setRequired('specification_item')
            ->setAllowedTypes('specification_item', [ProductVariantSpecificationItemInterface::class])
            ->setDefined('specification')
            ->setAllowedTypes('specification', ['null', ProductVariantSpecificationInterface::class])
            ->setDefaults([
                'label' => function (Options $options): string {
                    return $options['specification_item']->getCode();
                },
            ]);
    }

    public function getBlockPrefix(): string
    {
        return 'dh_product_variant_specification_item_configuration';
    }
}
