<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Form\Type;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationInterface;
use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationItemInterface;
use DH\ArtisProductSpecificationPlugin\Form\Type\Translation\ProductVariantSpecificationTranslationType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

final class ProductVariantSpecificationType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('code', TextType::class, [
                'label' => 'dh_artis_product_specification_plugin.form.product_variant.specification.code',
                'validation_groups' => ['artis'],
                'disabled' => true
            ])
            ->add('translations', ResourceTranslationsType::class, [
                'label' => 'dh_artis_product_specification_plugin.form.product_variant_specification.translations',
                'entry_type' => ProductVariantSpecificationTranslationType::class,
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            /** @var ProductVariantSpecificationInterface $specification */
            $specification = $event->getData();

            if (null !== $specification) {
                $event->getForm()->add('itemConfigurations', ProductVariantSpecificationItemConfigurationCollectionType::class, [
                    'option' => $specification->getItems(),
                    'entry_type' => ProductVariantSpecificationItemConfigurationType::class,
                    'entry_options' => function (ProductVariantSpecificationItemInterface $productVariantSpecificationItem) use ($specification) {
                        return [
                            'specification_item' => $productVariantSpecificationItem,
                            'specification' => $specification,
                            'required' => false,
                        ];
                    },
                    'label' => false,
                ]);
            }
        });
    }

    public function getBlockPrefix(): string
    {
        return 'dh_artis_product_variant_specification';
    }
}
