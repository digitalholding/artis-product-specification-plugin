<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\EventListener;

use DH\ArtisProductSpecificationPlugin\Entity\ProductVariantSpecificationImagesAwareInterface;
use Sylius\Component\Core\Uploader\ImageUploaderInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Webmozart\Assert\Assert;

final class ProductVariantSpecificationImagesUploadListener
{
    /** @var ImageUploaderInterface */
    private $uploader;

    public function __construct(ImageUploaderInterface $uploader)
    {
        $this->uploader = $uploader;
    }

    public function uploadImages(GenericEvent $event): void
    {
        $subject = $event->getSubject();
        Assert::isInstanceOf($subject, ProductVariantSpecificationImagesAwareInterface::class);

        $this->uploadSubjectImages($subject);
    }

    private function uploadSubjectImages(ProductVariantSpecificationImagesAwareInterface $subject): void
    {
        $images = $subject->getVariantSpecificationImages();

        foreach ($images as $image) {
            if ($image->hasFile()) {
                $this->uploader->upload($image);
            }

            if (null === $image->getPath()) {
                $images->removeElement($image);
            }
        }
    }
}
