<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\EventListener;

use Sylius\Component\Core\Model\ImageAwareInterface;
use Sylius\Component\Core\Uploader\ImageUploaderInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Webmozart\Assert\Assert;

final class ProductVariantSpecificationItemValueImageUploadListener
{
    /** @var ImageUploaderInterface */
    private $uploader;

    public function __construct(ImageUploaderInterface $uploader)
    {
        $this->uploader = $uploader;
    }

    public function uploadImage(GenericEvent $event): void
    {
        $values = $event->getSubject()->getValues();

        foreach ($values as $value) {
            Assert::isInstanceOf($value, ImageAwareInterface::class);
            $this->uploadSubjectImages($value);
        }
    }

    private function uploadSubjectImages(ImageAwareInterface $subject): void
    {
        $image = $subject->getImage();

        if (null !== $image) {
            $this->uploader->upload($image);
        }
    }
}
