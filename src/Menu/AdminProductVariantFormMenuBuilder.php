<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Menu;

use Sylius\Bundle\AdminBundle\Event\ProductVariantMenuBuilderEvent;

final class AdminProductVariantFormMenuBuilder
{
    public function addItems(ProductVariantMenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();

        $menu
            ->addChild('specification')
            ->setAttribute('template', '@DHArtisProductSpecificationPlugin/Admin/ProductVariant/Tab/_specification.html.twig')
            ->setLabel('dh_artis_product_specification_plugin.ui.specification');
    }
}
