<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Menu;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class SpecificationMenuBuilder
{
    public function addAdminMenuItems(MenuBuilderEvent $menuBuilderEvent): void
    {
        $menu = $menuBuilderEvent->getMenu();

        $submenu = $menu->getChild('catalog');

        $submenu
            ->addChild('product_variant_specification_items', [
                'route' => 'dh_artis_product_specification_plugin_admin_product_variant_specification_item_index',
            ])
            ->setLabel('dh_artis_product_specification_plugin.ui.product_variant_specification_items')
            ->setLabelAttribute('icon', 'file alternate')
        ;
    }
}
