<?php

declare(strict_types=1);

namespace DH\ArtisProductSpecificationPlugin\Menu;

use Sylius\Bundle\AdminBundle\Event\ProductMenuBuilderEvent;

final class AdminProductFormMenuBuilder
{
    public function addItems(ProductMenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();

        $menu
            ->addChild('specification')
            ->setAttribute('template', '@DHArtisProductSpecificationPlugin/Admin/Product/Tab/_specification.html.twig')
            ->setLabel('dh_artis_product_specification_plugin.ui.specification');
    }
}
